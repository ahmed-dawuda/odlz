package model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "zones")
@Inheritance
public class Zone extends Location {

    @OneToMany(mappedBy = "zone", fetch = FetchType.LAZY)
    private List<Cell> cells = new ArrayList<>();

    @OneToMany(mappedBy = "zone", fetch = FetchType.LAZY)
    private List<Transfer> zones;

    public List<Cell> getCells() {
        return cells;
    }

    public void setCells(List<Cell> cells) {
        this.cells = cells;
    }
}
