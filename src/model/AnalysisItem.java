package model;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "analysisitems")
public class AnalysisItem {

    @Id
    @Column
    private Number id;

    @JoinColumn
    @ManyToOne
    private Analysis analysis;

    @ManyToOne
    @JoinColumn(name = "product")
    private Product product;

    @Column(name = "stockBalAsAt")
    private Date stockBalAsAt;

    @Column(name = "supplyOn")
    private double supplyOn;

    @Column(name = "totalStockAsAt")
    private Date totalStockAsAt;

    @Column(name = "currentStock")
    private double currentStock;

    @Column(name = "quantity")
    private double quantity;

    @Column(name = "unitPrice")
    private double unitPrice;

    @Column(name = "amount")
    private double amount;

    public Number getId() {
        return id;
    }

    public void setId(Number id) {
        this.id = id;
    }

    public Analysis getAnalysis() {
        return analysis;
    }

    public void setAnalysis(Analysis analysis) {
        this.analysis = analysis;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Date getStockBalAsAt() {
        return stockBalAsAt;
    }

    public void setStockBalAsAt(Date stockBalAsAt) {
        this.stockBalAsAt = stockBalAsAt;
    }

    public double getSupplyOn() {
        return supplyOn;
    }

    public void setSupplyOn(double supplyOn) {
        this.supplyOn = supplyOn;
    }

    public Date getTotalStockAsAt() {
        return totalStockAsAt;
    }

    public void setTotalStockAsAt(Date totalStockAsAt) {
        this.totalStockAsAt = totalStockAsAt;
    }

    public double getCurrentStock() {
        return currentStock;
    }

    public void setCurrentStock(double currentStock) {
        this.currentStock = currentStock;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
