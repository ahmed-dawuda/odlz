package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;
import model.Invoice;
import model.Product;

import java.sql.Date;
import java.util.List;

public class Sale {

    public model.Sale create(double amount, model.Invoice invoice, Product product, double quantity){

        model.Sale sale = new model.Sale();
        sale.setId(GlobalValues.generateId());
        sale.setAmount(amount);
        sale.setInvoice(invoice);
        sale.setPrice(product.getPrice());
        sale.setProduct(product);
        sale.setQuantity(quantity);
        DBManager.save(model.Sale.class, sale);

        return sale;
    }

    public List<model.Sale> getSalesByInvoice(Invoice invoice){
        return DBManager.query(model.Sale.class, "select s from Sale s where s.invoice = ?1", invoice);
    }

}
