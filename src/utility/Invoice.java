package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.Credit;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

public class Invoice {

    CashBook cashBookUtil = new CashBook();

    public List<model.Invoice> all(){
        return DBManager.query(model.Invoice.class, "SELECT i FROM Invoice i WHERE i.worker = ?1", GlobalValues.getWorker());
    }

    public model.Invoice create(String customername, double discount, double payable, double total, String type){
        model.Invoice invoice = new model.Invoice();
        invoice.setId(GlobalValues.generateId());
        invoice.setDate(new Date(System.currentTimeMillis()));
        invoice.setTime(new Time(System.currentTimeMillis()));
        invoice.setCustomer(customername.trim());
        invoice.setDiscount(discount);
        invoice.setPayable(payable);
        invoice.setTotal(total);
        invoice.setType(type);
        invoice.setWorker(GlobalValues.getWorker());
        invoice.setLocation(GlobalValues.getZone());
        DBManager.save(model.Invoice.class, invoice);
        Credit credit = cashBookUtil.credit(payable, "cash sales to "+ customername);
        DBManager.begin();
        GlobalValues.getWorker().getCredits().add(credit);
        DBManager.commit();
        return invoice;
    }

    public List<model.Invoice> filterByDates(Date start, Date end){
        return DBManager.query(model.Invoice.class, "select i from Invoice i where i.date >= ?1 and i.date <= ?2 and i.location = ?3", start, end, GlobalValues.getZone());
    }
}
