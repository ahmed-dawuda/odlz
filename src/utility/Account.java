package utility;


import mStuff.DBManager;
import model.*;

import java.util.List;

public class Account {

    public List<model.Account> all(){
        return DBManager.listAll(model.Account.class);
    }

    public void update(model.Account account, model.Payment payment){
        DBManager.begin();
        account.getPayments().add(payment);
        DBManager.commit();
    }
}
