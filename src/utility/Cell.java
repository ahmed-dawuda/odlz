package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;

import java.util.List;

public class Cell {

    private Inventory inventoryUtil = new Inventory();

    public List<model.Cell> all(){
        return DBManager.query(model.Cell.class, "SELECT c FROM Cell c WHERE c.worker = ?1", GlobalValues.getWorker());
    }

    public model.Cell create(String name, String caretaker, String status, String phone, String Idtype, String Idnumber, String landmark, String dac, String district, String region){
        model.Cell cell = new model.Cell();
        cell.setId(GlobalValues.generateId());
        cell.setName(name);
        cell.setCaretaker(caretaker);
        cell.setStatus(status);
        cell.setPhone(phone);
        cell.setIdType(Idtype);
        cell.setIdNumber(Idnumber);
        cell.setLandmark(landmark);
        cell.setDistrict(district);
        cell.setRegion(region);
        cell.setDac(dac);
        cell.setWorker(GlobalValues.getWorker());
        cell.setZone(GlobalValues.getZone());
        DBManager.save(model.Cell.class, cell);
//        List<Product> list = inventoryUtil.products();
//        for (Product product : list) {
//            inventoryUtil.createInventoryItem(cell, product, 0);
//        }
        return cell;
    }

    public model.Cell update(model.Cell cell, String name, String caretaker, String status, String phone, String Idtype, String Idnumber, String landmark, String dac, String district, String region){
        DBManager.begin();
        cell.setName(name);
        cell.setCaretaker(caretaker);
        cell.setStatus(status);
        cell.setPhone(phone);
        cell.setIdType(Idtype);
        cell.setIdNumber(Idnumber);
        cell.setLandmark(landmark);
        cell.setDac(dac);
        cell.setDistrict(district);
        cell.setRegion(region);
        DBManager.commit();
        return cell;
    }

}
