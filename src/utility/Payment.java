package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

public class Payment {
    private CashBook cashBookUtil = new CashBook();

    public model.Payment create(model.Account bank, double amount) {
        model.Payment payment = new model.Payment();
        payment.setId(GlobalValues.generateId());
        payment.setAccount(bank);
        payment.setAmount(amount);
        payment.setDate(new Date(System.currentTimeMillis()));
        payment.setTime(new Time(System.currentTimeMillis()));
        payment.setWorker(GlobalValues.getWorker());
        DBManager.save(model.Payment.class, payment);
        Debit debit = cashBookUtil.debit(amount, "payment to bank: " + bank.getBank());
        DBManager.begin();
        GlobalValues.getWorker().getDebits().add(debit);
        DBManager.commit();
        return payment;
    }

    public List<model.Payment> all(){
        return DBManager.query(model.Payment.class, "SELECT p FROM Payment p where p.worker = ?1", GlobalValues.getWorker());
    }

    public List<model.Payment> filterByDates(Date start, Date end){
        return DBManager.query(model.Payment.class, "select p from Payment p where p.date >= ?1 and p.date <= ?2 and p.location = ?3", start, end, GlobalValues.getZone());
    }
}
