package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.*;
import model.Cell;
import model.Product;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Inventory {
    private utility.Product productUtil = new utility.Product();

    public List<model.Inventory> all(){
        return DBManager.listAll(model.Inventory.class);
    }

    public List<model.Inventory> zonalInventory(Zone zone){
        return DBManager.query(model.Inventory.class, "SELECT i FROM Inventory i where i.location = ?1", zone);
    }

    /**
     *
     * @return products that are in the inventory. similar to select product from inventories
     */
    public List<model.Product> products(){
        return DBManager.listAll(Product.class);
    }

    public model.Inventory getInventoryByProduct(Product product){
        String query = "select i from Inventory i where i.product = ?1 and i.location = ?2";
        List<model.Inventory> inventories = DBManager.query(model.Inventory.class, query, product, GlobalValues.getZone());
        model.Inventory inventory = null;
        if (inventories.size() > 0) {
            inventory = inventories.get(0);
        }
        return inventory;
    }

    public model.Inventory createInventoryItem(Cell cell, Product product, double quantity){
        model.Inventory inventory = new model.Inventory();
        inventory.setProduct(product);
        inventory.setId(GlobalValues.generateId());
        inventory.setLastUpdated(new Date(System.currentTimeMillis()));
        inventory.setQuantity(quantity);
        inventory.setLocation(cell);
        cell.getInventory().add(inventory);
        DBManager.save(model.Inventory.class, inventory);
        return inventory;
    }

    public model.Inventory getCellInventoryByProduct(Cell cell, Product product){
        List<model.Inventory> query = DBManager.query(model.Inventory.class, "SELECT i FROM Inventory i where i.location = ?1 and i.product = ?2", cell, product);
        if (query.size() > 0) return query.get(0);
        return null;
    }

    public model.Inventory getZoneInventoryByProduct(Zone zone, Product product) {
        List<model.Inventory> list = DBManager.query(model.Inventory.class, "SELECT i FROM Inventory i where i.location = ?1 AND i.product = ?2", zone, product);
        if (list.size() > 0) return list.get(0);
        return null;
    }

    public void updateInventory(Location location, Product product, double quantity){
        List<model.Inventory> inventoryList = DBManager.query(model.Inventory.class, "SELECT i FROM Inventory i where i.location = ?1 AND i.product = ?2", location, product);
        model.Inventory inventory = inventoryList.size() > 0 ? inventoryList.get(0) : null;
        if (inventory != null) {
            DBManager.begin();
            inventory.setPrevQty(inventory.getQuantity());
            inventory.setQuantity(quantity + inventory.getQuantity());
            inventory.setLastUpdated(new Date(System.currentTimeMillis()));
            DBManager.commit();
        }
    }
}
