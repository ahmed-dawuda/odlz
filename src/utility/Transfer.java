package utility;


import mStuff.DBManager;
import mStuff.GlobalValues;
import model.Good;
import model.GoodReceived;
import model.Issue;
import model.Requisition;

import java.sql.Date;
import java.sql.Time;
import java.util.List;

public class Transfer {

    public model.Transfer create(){
        Requisition requisition = new Requisition();
        requisition.setId(GlobalValues.generateId());
        requisition.setDate(new Date(System.currentTimeMillis()));
        requisition.setTime(new Time(System.currentTimeMillis()));
        requisition.setWorker(GlobalValues.getWorker());
        requisition.setZone(GlobalValues.getZone());
        DBManager.save(Requisition.class, requisition);
        return requisition;
    }

    public List<Requisition> allRequisitions(){
        return DBManager.query(Requisition.class, "Select r from Requisition r where r.zone = ?1 ORDER BY r.date DESC", GlobalValues.getZone());
    }

    public List<Good> getGoodsByRequisition(Requisition requisition){
        return DBManager.query(Good.class, "select g from Good g where g.transfer = ?1", requisition);
    }

    public String getStatus(model.Transfer originator){
        String status = "Pending";

        long integer = DBManager.queryForSingleResult(Long.class, "select count(g.id) from GoodReceived g where g.originator = ?1", originator);
        if (integer > 0) {
            return "Completed";
        }

        long integer2 = DBManager.queryForSingleResult(Long.class, "select count(g.id) from Issue g where g.originator = ?1", originator);
        if (integer2 > 0) {
            return "Issued";
        }

        long integer1 = DBManager.queryForSingleResult(Long.class, "select count(g.id) from IssueCommand g where g.originator = ?1", originator);
        if (integer1 > 0) {
            return "Approved";
        }

        return status;
    }

    public List<Issue> issues(){
        return DBManager.query(Issue.class, "SELECT i FROM Issue i where i.zone = ?1 ORDER BY i.date DESC", GlobalValues.getZone());
    }

    public GoodReceived createGoodReceived(model.Transfer originator){
        GoodReceived goodReceived = new GoodReceived();
        goodReceived.setId(GlobalValues.generateId());
        goodReceived.setWorker(originator.getWorker());
        goodReceived.setTime(new Time(System.currentTimeMillis()));
        goodReceived.setZone(originator.getZone());
        goodReceived.setDate(new Date(System.currentTimeMillis()));
        goodReceived.setOriginator(originator);
        DBManager.save(GoodReceived.class, goodReceived);
        return goodReceived;
    }

    public List<GoodReceived> receivedGoods(){
        return DBManager.query(GoodReceived.class, "select g from GoodReceived g where g.zone = ?1 order by g.date desc", GlobalValues.getZone());
    }
}
