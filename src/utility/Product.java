package utility;


import mStuff.DBManager;

import java.util.List;

public class Product {

    public List<model.Product> all(){
        return DBManager.listAll(model.Product.class);
    }
}
