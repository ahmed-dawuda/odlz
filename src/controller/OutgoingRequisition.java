package controller;


import controller.popups.NewRequisition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;
import model.Product;
import model.Requisition;
import utility.*;
import utility.Transfer;

import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

public class OutgoingRequisition {
    /**
     * declaring tables and fields within the view
     */
    @FXML private TableView<model.Requisition> requestTable;
    @FXML private TableView<Good> requestDetailTable;

    /**
     * columns in the request table
     */
    @FXML private TableColumn<Requisition, Date> dateCol;
    @FXML private TableColumn<model.Requisition, Time> timeCol;
    @FXML private TableColumn<Requisition, Number> statusCol;

    /**
     * columns in the request detail table
     */
    @FXML private TableColumn<Good, Product> productCol;
    @FXML private TableColumn<Good, Double> quantityCol;
    @FXML private TableColumn<Good, Product> priceCol;

    private utility.Transfer transferUtil = new Transfer();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(dateCol, timeCol, statusCol), Arrays.asList("date", "time", "id"));
        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, priceCol), Arrays.asList("product", "quantity", "product"));

        statusCol.setCellFactory(c -> new TableCell<Requisition, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText(null);
                } else {
                    int index = getTableRow().getIndex();
                    Requisition requisition = requestTable.getItems().get(index);
                    setText(transferUtil.getStatus(requisition));
                }
            }
        });

        dateCol.setCellFactory(cell -> new TableCell<Requisition, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
            }
        });

        timeCol.setCellFactory(cell -> new TableCell<Requisition, Time>(){
            @Override
            protected void updateItem(Time item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getTimeInstance().format(item));
            }
        });

        productCol.setCellFactory(cell -> new TableCell<Good, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        quantityCol.setCellFactory(cell -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item ==  null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        priceCol.setCellFactory(cell -> new TableCell<Good, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item.getPrice()));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        requestDetailTable.setItems(FXCollections.observableArrayList());

        requestTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            requestDetailTable.getItems().clear();
            List<Good> goods = newValue.getGoods();
            if (goods.size() == 0) {
                goods = transferUtil.getGoodsByRequisition(newValue);
            }
            requestDetailTable.getItems().addAll(goods);
        });

        requestTable.setItems(FXCollections.observableArrayList(transferUtil.allRequisitions()));
        requestTable.getSelectionModel().selectFirst();
    }

    public void makeRequisition(){
        FXMLLoader modal = ViewUtility.modal("popups/makerequisition", "Make a new requisition", true);
        NewRequisition controller = modal.getController();
        controller.init(requestTable.getItems());
    }

    public void filter(ActionEvent actionEvent) {

    }
}
