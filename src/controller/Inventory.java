package controller;


import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Product;

import java.text.DecimalFormat;
import java.util.Arrays;

public class Inventory {

    @FXML private TableView<model.Inventory> inventoryTable;
    @FXML private TableColumn<model.Inventory, Product> productCol;
    @FXML private TableColumn<model.Inventory, Double> quantityCol;
    @FXML private TableColumn<model.Inventory, Product> priceCol;
    @FXML private TableColumn<model.Inventory, Double> amountCol;

    @FXML private TextField totalAmount;

    private utility.Inventory inventoryUtil = new utility.Inventory();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, priceCol), Arrays.asList("product", "quantity", "product"));
        amountCol.setCellValueFactory(cellData -> {
            double amount = cellData.getValue().getQuantity() * cellData.getValue().getProduct().getPrice();
            return new SimpleObjectProperty<>(amount);
        });


        productCol.setCellFactory(cell -> new TableCell<model.Inventory, Product>() {
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        quantityCol.setCellFactory(cell -> new TableCell<model.Inventory, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item ==  null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
                if (item != null && !empty) {
                    int index = getTableRow().getIndex();
                    model.Inventory inventory = inventoryTable.getItems().get(index);
                    setStyle(item < inventory.getMinStock() ? "-fx-font-weight: bold; -fx-text-fill: #e65100" : "-fx-font-weight: normal");
                }
            }
        });

        priceCol.setCellFactory(cell -> new TableCell<model.Inventory, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item.getPrice()));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        amountCol.setCellFactory(cell -> new TableCell<model.Inventory, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        inventoryTable.setItems(FXCollections.observableArrayList(GlobalValues.getZone().getInventory()));

        double total = 0;

        for (model.Inventory inventory : inventoryTable.getItems()) {
            total += (inventory.getQuantity() * inventory.getProduct().getPrice());
        }
        totalAmount.setText(GlobalValues.getCurrencyInstance().format(total));
    }
}
