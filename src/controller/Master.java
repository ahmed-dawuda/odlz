package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Product;

import java.io.IOException;
import java.util.List;

public class Master {


    public VBox content;
    public Button requisitions;
    public Button sales;
    public Button payments;
    public Button inventory;
    public Label header;

    public void initialize() throws IOException {
        loadContent("inventory");
    }

    public void changeContent(ActionEvent event) throws IOException {
        String text = ((Button) event.getSource()).getId().toLowerCase();

        if(text.equalsIgnoreCase("logout")){
//            System.out.println(text);
            ViewUtility.modal("popups/logout", "Desert Lion Group");
        }else {
            loadContent(text);
        }
    }

    public void loadContent(String text) throws IOException {
        header.setText("Desert Lion Group > " + text.toUpperCase());
        Parent load = FXMLLoader.load(getClass().getResource("/view/template/content/" + text + ".fxml"));

        content.getChildren().clear();
        VBox.setVgrow(load, Priority.ALWAYS);
        content.getChildren().add(load);
    }
}
