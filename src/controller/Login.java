package controller;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import mStuff.ViewUtility;
import utility.Worker;

import java.io.IOException;

public class Login {

    @FXML private TextField username;
    @FXML private PasswordField password;

    private Worker workerUtil = new Worker();

    public void initialize(){

    }

    public void login() throws IOException {
        if (username.getText().trim().isEmpty() || password.getText().trim().isEmpty()) {
            ViewUtility.alert("No username or password", "Please make sure all fields are provided");
        } else {
            if (workerUtil.login(username.getText().trim(), password.getText().trim())) {
                Stage window = (Stage) username.getScene().getWindow();
                EventHandler<WindowEvent> onCloseRequest = window.getOnCloseRequest();
                window.setOnCloseRequest(null);
                this.quit();
                ViewUtility.newWindow("master", "Desert Lion Group | Zonal Officer", onCloseRequest);
            } else {
                ViewUtility.alert("Login Error", "Your credentials are incorrect or You are not authorized to use the system");
            }
        }
    }

    public void quit(){
        ViewUtility.closeWindow(username);
    }
}
