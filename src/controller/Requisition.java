package controller;

import controller.popups.NewRequisition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import mStuff.ViewUtility;
import model.Good;
import model.Product;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.util.Arrays;
import java.util.List;

public class Requisition {

    @FXML private VBox holder;

    public void initialize() throws IOException {
        loadPage("outgoing");
    }

    public void changeRequisition(ActionEvent event) throws IOException {
        String text = ((Button) event.getSource()).getId().toLowerCase();
        loadPage(text);

    }

    public void loadPage(String filename) throws IOException {
        Parent load = FXMLLoader.load(getClass().getResource("/view/template/content/includes/" + filename + ".fxml"));
        holder.getChildren().clear();
        VBox.setVgrow(load, Priority.ALWAYS);
        holder.getChildren().add(load);
    }
}
