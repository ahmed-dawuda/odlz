package controller;


import controller.popups.GoodReceivedNote;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Good;
import model.Issue;
import model.Product;
import model.Transfer;
import org.eclipse.persistence.annotations.Array;

import java.sql.Date;
import java.sql.Time;
import java.util.Arrays;

public class IncomingRequisition {

    @FXML private TableView<Issue> requestTable;
    @FXML private TableColumn<Issue, Date> dateCol;
    @FXML private TableColumn<Issue, Time> timeCol;
    @FXML private TableColumn<Issue, Number> statusCol;

    @FXML private TableView<Good> goodsTable;
    @FXML private TableColumn<Good, Product> productCol;
    @FXML private TableColumn<Good, Double> quantityCol;
    @FXML private TableColumn<Good, Double> instockCol;

    private utility.Transfer transferUtil = new utility.Transfer();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(dateCol, timeCol, statusCol), Arrays.asList("date", "time", "id"));
        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, instockCol), Arrays.asList("product", "quantity", "instock"));

        dateCol.setCellFactory(c -> new TableCell<Issue, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
            }
        });

        timeCol.setCellFactory(c -> new TableCell<Issue, Time>(){
            @Override
            protected void updateItem(Time item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getTimeInstance().format(item));
            }
        });

        statusCol.setCellFactory(c -> new TableCell<Issue, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null || empty) {
                    setText(null);
                } else {
                    int index = getTableRow().getIndex();
                    Transfer transfer = requestTable.getItems().get(index);
                    setText(transferUtil.getStatus(transfer.getOriginator()));
                }
            }
        });

        productCol.setCellFactory(c -> new TableCell<Good, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        quantityCol.setCellFactory(c -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
            }
        });

        instockCol.setCellFactory(c -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ?  null : GlobalValues.getDefaultDecimalFormat().format(item));
            }
        });

        goodsTable.setItems(FXCollections.observableArrayList());

        requestTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            goodsTable.getItems().clear();
            goodsTable.getItems().addAll(newValue.getGoods());
        });

        requestTable.setItems(FXCollections.observableArrayList(transferUtil.issues()));
        requestTable.getSelectionModel().selectFirst();
    }

    public void receivedNote(){
        Issue selectedItem = requestTable.getSelectionModel().getSelectedItem();
        String status = transferUtil.getStatus(selectedItem.getOriginator());
        if (!status.equalsIgnoreCase("Issued")){
            ViewUtility.alert("Oops", "This request has already been received");
        } else {
            FXMLLoader modal = ViewUtility.modal("popups/receivenote", "Goods received note");
            GoodReceivedNote controller = modal.getController();
            controller.init(selectedItem);
        }
    }

    public void filter(){

    }
}
