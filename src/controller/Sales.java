package controller;

import controller.popups.DetailSale;
import controller.popups.NewSale;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;

import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class Sales {

    @FXML private TableView<Invoice> salesTable;
    @FXML private TableColumn<Invoice, Date> dateCol;
    @FXML private TableColumn<Invoice, Time> timeCol;
    @FXML private TableColumn<Invoice, String> customerCol;
    @FXML private TableColumn<Invoice, Double> amountCol;
    @FXML private TableColumn<Invoice, Double> discountCol;
    @FXML private TableColumn<Invoice, Double> netCol;
    @FXML private TableColumn<Invoice, Number> viewCol;

    @FXML private DatePicker startDate;
    @FXML private DatePicker endDate;

    @FXML private TextField totalSales;
    @FXML private TextField totalDiscount;
    @FXML private TextField netSales;

    private utility.Invoice invoiceUtil = new utility.Invoice();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(dateCol, timeCol, customerCol, amountCol, discountCol, netCol, viewCol), Arrays.asList("date", "time", "customer", "total", "discount", "payable", "id"));

        viewCol.setCellFactory(cell ->  new TableCell<Invoice, Number>() {
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Detail");
                link.setOnAction(event -> {
                    int index = getTableRow().getIndex();
                    FXMLLoader modal = ViewUtility.modal("popups/detailSale", "Invoice", true);
                    DetailSale detailSaleController = modal.getController();
                    detailSaleController.init(salesTable.getItems().get(index));
                });
                link.setTextFill(Color.GREEN);
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });


        dateCol.setCellFactory(cell -> new TableCell<Invoice, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
            }
        });

        timeCol.setCellFactory(cell -> new TableCell<Invoice, Time>(){
            @Override
            protected void updateItem(Time item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getTimeInstance().format(item));
            }
        });

        amountCol.setCellFactory(cell -> new TableCell<Invoice, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        discountCol.setCellFactory(cell -> new TableCell<Invoice, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        netCol.setCellFactory(cell -> new TableCell<Invoice, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });


        salesTable.itemsProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                totalSales.setText("0");
                totalDiscount.setText(GlobalValues.getCurrencyInstance().format("0"));
                netSales.setText(GlobalValues.getCurrencyInstance().format("0"));
            }
            else {
                double totalsales = 0, totaldiscount = 0, netsales = 0;
                for (model.Invoice invoice : newValue) {
                    totalsales += invoice.getTotal();
                    totaldiscount += invoice.getDiscount();
                    netsales += invoice.getPayable();
                }
                totalSales.setText(GlobalValues.getCurrencyInstance().format(totalsales));
                totalDiscount.setText(GlobalValues.getCurrencyInstance().format(totaldiscount));
                netSales.setText(GlobalValues.getCurrencyInstance().format(netsales));
            }
        });


        salesTable.setItems(FXCollections.observableArrayList(invoiceUtil.all()));

        salesTable.getItems().addListener((ListChangeListener<Invoice>) c -> {
            ObservableList<? extends model.Invoice> list = c.getList();
            double totalsales = 0, totaldiscount = 0, netsales = 0;
            for (Invoice invoice : list) {
                totalsales += invoice.getTotal();
                totaldiscount += invoice.getDiscount();
                netsales += invoice.getPayable();
            }
            totalSales.setText(GlobalValues.getCurrencyInstance().format(totalsales));
            totalDiscount.setText(GlobalValues.getCurrencyInstance().format(totaldiscount));
            netSales.setText(GlobalValues.getCurrencyInstance().format(netsales));
        });
    }

    @FXML private void newSale() throws IOException {
        FXMLLoader modal = ViewUtility.modal("popups/addsale", "Make a new sale", true);
        NewSale newSaleController = modal.getController();
        newSaleController.init(salesTable.getItems());
    }

    public void filter(){
        LocalDate start = startDate.getValue() == null ? LocalDate.of(2015, 01, 01) : startDate.getValue();
        LocalDate end = endDate.getValue() == null ? LocalDate.now().plusDays(1) : endDate.getValue().plusDays(1);
        List<model.Invoice> sales = invoiceUtil.filterByDates(Date.valueOf(start), Date.valueOf(end));
        salesTable.getItems().clear();
        salesTable.getItems().addAll(sales);
//        System.out.println(sales);
    }
}
