package controller;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.io.IOException;

public class ManageCells {
    @FXML private VBox content;

    public void initialize() throws IOException {
        loadContent("inventory");
    }


    public void changeRequisition(ActionEvent event) throws IOException {
        String file = ((Button) event.getSource()).getId().toLowerCase();
        loadContent(file);
    }

    public void loadContent(String file) throws IOException {
        Parent load = FXMLLoader.load(getClass().getResource("/view/template/content/includes/" + file + ".fxml"));
        content.getChildren().clear();
        VBox.setVgrow(load, Priority.ALWAYS);
        content.getChildren().add(load);
    }
}
