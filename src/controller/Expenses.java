package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import mStuff.ViewUtility;


public class Expenses {
    public TableView expenseTable;
    public TableColumn timeCol;
    public TableColumn purposeCol;
    public TableColumn amountCol;
    public TableColumn dateCol;

    public void filter(ActionEvent event) {

    }

    public void makeExpense(ActionEvent event) {
        FXMLLoader modal = ViewUtility.modal("popups/addExpense", "Add New Expense", true);
    }
}
