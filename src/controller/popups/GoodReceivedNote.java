package controller.popups;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;
import model.Product;
import utility.*;
import utility.Inventory;
import utility.Transfer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GoodReceivedNote {

    @FXML private TextField product;
    @FXML private TextField qtyGood;
    @FXML private TextField qtyBad;
    @FXML private TextField totalQty;

    @FXML private TableView<TransferStatus> goodsTable;
    @FXML private TableColumn<TransferStatus, Product> productCol;
    @FXML private TableColumn<TransferStatus, Double> goodCol;
    @FXML private TableColumn<TransferStatus, Double> badCol;
    @FXML private TableColumn<TransferStatus, Double> totalCol;
    @FXML private TableColumn<TransferStatus, Number> editCol;

    private TransferStatus selectedGood = null;
    private Issue issue = null;
    private Transfer transferUtil = new Transfer();
    private utility.Inventory inventoryUtil = new Inventory();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(productCol, goodCol, badCol, totalCol, editCol), Arrays.asList("product", "good", "bad", "total", "id"));

//        qtyGood.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (newValue.isEmpty() || newValue.equalsIgnoreCase("0")) return;
//            try {
//                double g = Double.parseDouble(newValue);
//                qtyBad.setText(GlobalValues.getDefaultDecimalFormat().format(selectedGood.getTotal() - selectedGood.getGood()));
//            } catch (Exception e){
//                qtyGood.setText(oldValue);
//            }
//        });

        product.setAlignment(Pos.BASELINE_LEFT);

        qtyBad.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()){
//                qtyBad.setText("0");
                qtyGood.setText(GlobalValues.getDefaultDecimalFormat().format(selectedGood.getGood()));
                return;
            }
            try {
                double b = Double.parseDouble(newValue);
                qtyGood.setText(GlobalValues.getDefaultDecimalFormat().format(selectedGood.getTotal() - b));
                if (b > selectedGood.getTotal()) {
                    qtyBad.setText(oldValue);
                }
            } catch (Exception e){
                qtyBad.setText(oldValue);
            }
        });

        productCol.setCellFactory(c -> new TableCell<TransferStatus, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
                setAlignment(Pos.BASELINE_LEFT);
            }
        });

        goodCol.setCellFactory(c -> new TableCell<TransferStatus, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        badCol.setCellFactory(c -> new TableCell<TransferStatus, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        totalCol.setCellFactory(c -> new TableCell<TransferStatus, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        editCol.setCellFactory(c -> new TableCell<TransferStatus, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Edit");
                link.setTextFill(Color.GREEN);
                link.setOnAction(e -> {
                    TransferStatus transferStatus = goodsTable.getItems().get(getTableRow().getIndex());
                    selectedGood = transferStatus;
                    product.setText(transferStatus.getProduct().getName());
                    qtyGood.setText(GlobalValues.getDefaultDecimalFormat().format(transferStatus.getGood()));
                    qtyBad.setText(transferStatus.getBad() + "");
                    totalQty.setText(GlobalValues.getDefaultDecimalFormat().format(transferStatus.getTotal()));
                });
                setAlignment(Pos.BASELINE_CENTER);
                setGraphic(item == null || empty ? null : link);
            }
        });
    }

    public void save(ActionEvent event) {

        ObservableList<TransferStatus> items = goodsTable.getItems();
        TransferStatus toRemove = null;

        for (TransferStatus item : items) {
            if (product.getText().trim().equalsIgnoreCase(item.getProduct().getName())){
                toRemove = item;
                break;
            }
        }

        if (selectedGood != null) {
            if (toRemove != null) {
                goodsTable.getItems().remove(toRemove);
            }

            TransferStatus transferStatus = new TransferStatus();
            transferStatus.setId(GlobalValues.generateId());
            transferStatus.setBad(Double.parseDouble(qtyBad.getText()));
            transferStatus.setGood(Double.parseDouble(qtyGood.getText()));
            transferStatus.setTotal(selectedGood.getTotal());
            transferStatus.setProduct(selectedGood.getProduct());

            goodsTable.getItems().remove(selectedGood);
            goodsTable.getItems().add(transferStatus);
        }
    }

    public void finish(ActionEvent event) {
        ObservableList<TransferStatus> transferStatuses = goodsTable.getItems();
        if (transferStatuses.size() > 0) {
            GoodReceived goodReceived = transferUtil.createGoodReceived(this.issue.getOriginator());
            ViewUtility.closeWindow(product);
            ViewUtility.alert("Goods received", "Your inventory has been updated with the good goods");
            for (TransferStatus transferStatus : transferStatuses) {
                transferStatus.setId(GlobalValues.generateId());
                transferStatus.setGoodsreceived(goodReceived);
                DBManager.save(TransferStatus.class, transferStatus);

                model.Inventory inventoryByProduct = inventoryUtil.getInventoryByProduct(transferStatus.getProduct());
                if (inventoryByProduct != null) {
                    DBManager.begin();
                    inventoryByProduct.setPrevQty(inventoryByProduct.getQuantity());
                    inventoryByProduct.setQuantity(inventoryByProduct.getQuantity() + transferStatus.getGood());
                    DBManager.commit();
                }
            }
            DBManager.begin();
            goodReceived.setTransferStatuses(transferStatuses);
            DBManager.commit();
        }
    }

    public void init(Issue issue){
        this.issue = issue;
        List<Good> goods = issue.getGoods();
        List<TransferStatus> transferStatuses = new ArrayList<>();

        for (Good good : goods) {
            transferStatuses.add(toTransferStatus(good));
        }

        goodsTable.getItems().clear();
        goodsTable.getItems().addAll(transferStatuses);
    }

    private TransferStatus toTransferStatus(Good good){
        TransferStatus transferStatus = new TransferStatus();
        transferStatus.setBad(0);
        transferStatus.setGood(good.getQuantity());
        transferStatus.setId(GlobalValues.generateId());
        transferStatus.setProduct(good.getProduct());
        transferStatus.setTotal(good.getQuantity());
        return transferStatus;
    }
}
