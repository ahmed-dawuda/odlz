package controller.popups;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Good;
import model.Issue;
import model.Product;
import model.Requisition;
import utility.Inventory;
import utility.Transfer;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

public class NewRequisition {

    @FXML private ChoiceBox<Product> productCB;
    @FXML private TextField quantityTF;
    @FXML private TextField inStock;
    @FXML private TextField totalAmount;

    @FXML private TableView<Good> goodsTable;
    @FXML private TableColumn<Good, Product> productCol;
    @FXML private TableColumn<Good, Double> quantityCol;
    @FXML private TableColumn<Good, Product> inStockCol;
    @FXML private TableColumn<Good, Number> controlsCol;

    /**
     * utility classes
     */
    private utility.Product productUtil = new utility.Product();
    private Inventory inventoryUtil = new Inventory();
    private Transfer transferUtil = new Transfer();

    List<Requisition> requisitions;

    public void initialize(){

        ViewUtility.initColumns(Arrays.asList(productCol, quantityCol, inStockCol, controlsCol), Arrays.asList("product", "quantity", "product", "id"));

        quantityTF.setText("0");

        productCol.setCellFactory(cell -> new TableCell<Good, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        quantityCol.setCellFactory(cell -> new TableCell<Good, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });


        inStockCol.setCellFactory(cell -> new TableCell<Good, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                model.Inventory inventory = inventoryUtil.getInventoryByProduct(item);
                if (inventory != null) {
                    setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(inventory.getQuantity()));
                } else {
                    setText(item == null || empty ? null : "0");
                }
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        controlsCol.setCellFactory(cell -> new TableCell<Good, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink remove = new Hyperlink("Remove");
                remove.setOnAction(event -> {
                    goodsTable.getItems().remove(getTableRow().getIndex());
                });
                remove.setTextFill(Color.valueOf("#e65100"));
                setGraphic(item == null || empty ? null : remove);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        productCB.setConverter(new StringConverter<Product>() {
            @Override
            public String toString(Product object) {
                return object.getName();
            }

            @Override
            public Product fromString(String string) {
                return null;
            }
        });


        quantityTF.textProperty().addListener((observable, oldValue, newValue) -> {
            double qty;
            if (newValue.isEmpty()) {
                totalAmount.setText("0");
                return;
            }
            try{
                model.Inventory inventory = inventoryUtil.getInventoryByProduct(productCB.getSelectionModel().getSelectedItem());
                qty = Double.parseDouble(newValue);
                totalAmount.setText(GlobalValues.getDefaultDecimalFormat().format((qty + inventory.getQuantity())));
            }catch (Exception ex){
                quantityTF.setText(oldValue);
            }
        });

        productCB.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Product>() {
            @Override
            public void changed(ObservableValue<? extends Product> observable, Product oldValue, Product newValue) {
                model.Inventory inventory = inventoryUtil.getInventoryByProduct(newValue);
                inStock.setText(GlobalValues.getDefaultDecimalFormat().format(inventory.getQuantity()));
                totalAmount.setText(GlobalValues.getDefaultDecimalFormat().format(Double.parseDouble(quantityTF.getText()) * newValue.getPrice()));
            }
        });

        productCB.setItems(FXCollections.observableArrayList(productUtil.all()));
        productCB.getSelectionModel().selectFirst();
        goodsTable.setItems(FXCollections.observableArrayList());
    }

    public void addGood(){
        if (quantityTF.getText().trim().equalsIgnoreCase("0")){
            ViewUtility.alert("Quantity can not be 0", "Please request for a valid quantity, not 0");
            return;
        }
        List<Good> goods = goodsTable.getItems();

        for (Good good : goods) {
            if (good.getProduct().getName().equalsIgnoreCase(productCB.getSelectionModel().getSelectedItem().getName())) {
                ViewUtility.alert("Product already exist", "You've already added this item. Please remove it before adding again");
                goodsTable.getSelectionModel().select(good);
                return;
            }
        }

        Good good = new Good();
        good.setId(GlobalValues.generateId());
        good.setQuantity(Double.parseDouble(quantityTF.getText()));
        good.setProduct(productCB.getSelectionModel().getSelectedItem());
        model.Inventory inventory = inventoryUtil.getZoneInventoryByProduct(GlobalValues.getZone(), good.getProduct());
        good.setInstock(inventory.getQuantity());
        good.setQuantityRequested(good.getQuantity());
        goodsTable.getItems().add(good);

    }

    public void request(){
        List<Good> goods = goodsTable.getItems();
        model.Transfer transfer = transferUtil.create();

        for (Good good : goods) {
            good.setTransfer(transfer);
            DBManager.save(Good.class, good);
        }

        DBManager.begin();
        transfer.setGoods(goods);
        DBManager.commit();

        this.requisitions.add(((Requisition) transfer));

        productCB.getSelectionModel().selectFirst();
        quantityTF.setText("0");
        goodsTable.getItems().clear();

        ViewUtility.alert("Success", "Your requisition has been sent for reviewing");

        ViewUtility.closeWindow(productCB);
    }

    public void init(List<Requisition> requisitions){
        this.requisitions = requisitions;
    }
}
