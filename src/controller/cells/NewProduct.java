package controller.cells;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.util.StringConverter;
import mStuff.ViewUtility;
import model.*;
import model.Product;
import utility.Analysis;
import utility.Inventory;

import java.sql.Date;
import java.util.List;

public class NewProduct {

    @FXML private ChoiceBox<Product> product;
    private List<AnalysisItem> analysisItemsTable;
    private utility.Product productUtil = new utility.Product();
    private utility.Analysis analysisUtil = new Analysis();
    private Inventory inventoryUtil = new Inventory();

    private model.Cell cell = null;


    public void initialize(){
        product.setConverter(new StringConverter<Product>() {
            @Override
            public String toString(Product object) {
                return object.getName();
            }

            @Override
            public Product fromString(String string) {
                return null;
            }
        });

        product.setItems(FXCollections.observableArrayList(inventoryUtil.products()));
        product.getSelectionModel().selectFirst();
    }

    public void init(List<AnalysisItem> items, model.Cell cell){
        analysisItemsTable = items;
        this.cell = cell;
    }

    public void add(){
        if (product.getSelectionModel().getSelectedItem() == null) {
            ViewUtility.alert("Please select a product", "you must select a product first");
            return;
        }

        for (AnalysisItem item : analysisItemsTable) {
            if (item.getProduct().getName().equalsIgnoreCase(product.getSelectionModel().getSelectedItem().getName())){
                ViewUtility.alert("Product already analyzed", "This product is already found inside this analysis sheet, please click edit beside the entry on the sheet to edit this item");
                return;
            }
        }
        Product prod = product.getSelectionModel().getSelectedItem();

        model.Inventory cellInventoryByProduct = inventoryUtil.getCellInventoryByProduct(this.cell, prod);
//        System.out.println(cellInventoryByProduct.getProduct().getName());
        AnalysisItem analysisItem = analysisUtil.createAnalysisItem(null, product.getSelectionModel().getSelectedItem(), cellInventoryByProduct.getQuantity(), cellInventoryByProduct.getQuantity(), new Date(System.currentTimeMillis()));
        analysisItemsTable.add(analysisItem);
    }

}
