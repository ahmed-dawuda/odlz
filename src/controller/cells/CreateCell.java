package controller.cells;


import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.util.StringConverter;
import mStuff.ViewUtility;
import model.*;
import model.Product;
import utility.*;
import utility.Cell;
import utility.Inventory;

import java.util.Arrays;
import java.util.List;

public class CreateCell {

    @FXML private TextField cellname;
    @FXML private TextField caretakername;
    @FXML private TextField status;
    @FXML private ChoiceBox<String> idtype;
    @FXML private TextField idnumber;
    @FXML private TextField landmark;
    @FXML private TextField digitaladdress;
    @FXML private TextField region;
    @FXML private TextField phone;
    @FXML private TextField district;

    private Cell cellUtil = new Cell();
    private List<model.Cell> cellsTable;
    private model.Cell editable = null;

    private utility.Inventory inventoryUtil = new Inventory();

    public void initialize(){
       idtype.setItems(FXCollections.observableArrayList(Arrays.asList("Passport", "Driver's License", "Voter's ID", "NHIS", "National ID")));
       idtype.getSelectionModel().selectFirst();
       digitaladdress.setText("000000");
    }

    public void save(){
        if (cellname.getText().trim().isEmpty()) {
            ViewUtility.alert("Name required", "Please provide the name of the cell");
            return;
        } else if (caretakername.getText().trim().isEmpty()) {
            ViewUtility.alert("Caretaker's name required", "You must provide, the name of the caretaker for this cell");
            return;
        } else if (status.getText().trim().isEmpty()) {
            ViewUtility.alert("Caretaker's Status required", "Please provide the status of the caretaker for this cell");
            return;
        } else if (phone.getText().trim().isEmpty()) {
            ViewUtility.alert("Caretaker's phone required", "Please provide the mobile number of the caretaker for this cell");
            return;
        } else if (idnumber.getText().trim().isEmpty()) {
            ViewUtility.alert("Caretaker's ID Number required", "Please provide the ID number of the caretaker for this cell");
            return;
        } else if (landmark.getText().trim().isEmpty()) {
            ViewUtility.alert("Landmark required", "Please provide the landmark for cell");
            return;
        } else if (region.getText().trim().isEmpty()) {
            ViewUtility.alert("Region required", "Please provide the region of the caretaker for this cell");
            return;
        } else if (digitaladdress.getText().trim().isEmpty()) {
            ViewUtility.alert("Digital Address required", "Please provide the digital Address  for this cell");
            return;
        } else if (district.getText().isEmpty()) {
            ViewUtility.alert("District is required", "Please provide the district  for this cell");
            return;
        }

        String cname = cellname.getText();
        String caretaker = caretakername.getText();
        String cstatus = status.getText();
        String cphone = phone.getText();
        String idn = idnumber.getText();
        String idt = idtype.getValue();
        String lndmrk = landmark.getText();
        String regn = region.getText();
        String dac = digitaladdress.getText();
        String dist = district.getText();


        if (editable == null) {
            model.Cell cell = cellUtil.create(cname, caretaker, cstatus, cphone, idt, idn, lndmrk, dac, dist, regn);
            this.cellsTable.add(cell);

            List<model.Product> products = inventoryUtil.products();

            for (Product product : products) {
                inventoryUtil.createInventoryItem(cell, product, 0);
            }
            ViewUtility.closeWindow(idtype);
        } else {
           cellUtil.update(editable, cname, caretaker, cstatus, cphone, idt, idn, lndmrk, dac, dist, regn);
            ViewUtility.closeWindow(this.idtype);
        }

    }

    public void init(List<model.Cell> cellsTable, model.Cell... cells){
        this.cellsTable = cellsTable;
        if (cells.length > 0) {
            this.editable = cells[0];
//            cell.setText(editable.getName());
//            caretaker.setText(editable.getCaretaker());
            phone.setText(editable.getPhone());
        }
    }

}
