package controller.cells;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Cell;
import model.Product;

import java.util.Arrays;

public class Inventory {

    public TableView<Cell> cellsTable;
    public TableColumn<Cell, String> nameCol;

    public TableView<model.Inventory> inventoryTable;
    public TableColumn<model.Inventory, Product> productCol;
    public TableColumn<model.Inventory, Product> priceCol;
    public TableColumn<model.Inventory, Double> stockCol;

    private utility.Cell cellUtil = new utility.Cell();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(productCol, priceCol, stockCol), Arrays.asList("product", "product", "quantity"));
        ViewUtility.initTableColumn(nameCol, "name");

        productCol.setCellFactory(c -> new TableCell<model.Inventory, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        priceCol.setCellFactory(c -> new TableCell<model.Inventory, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item.getPrice()));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        stockCol.setCellFactory(c -> new TableCell<model.Inventory, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
                if (item != null && !empty) {
                    int index = getTableRow().getIndex();
                    model.Inventory inventory = inventoryTable.getItems().get(index);
                    setStyle(item < inventory.getMinStock() ? "-fx-font-weight: bold; -fx-text-fill: #e65100" : "-fx-font-weight: normal");
                }
            }
        });

        cellsTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            inventoryTable.getItems().clear();
            inventoryTable.getItems().addAll(newValue.getInventory());
        });

        inventoryTable.setItems(FXCollections.observableArrayList());
        cellsTable.setItems(FXCollections.observableArrayList(cellUtil.all()));
        cellsTable.getSelectionModel().selectFirst();
    }

    public void updateInventory(){
        if (cellsTable.getSelectionModel().getSelectedItem() == null) {
            ViewUtility.alert("Selection is required", "Please select a cell you want to update it's inventory");
            return;
        }
        FXMLLoader modal = ViewUtility.modal("popups/updateinventory", "Update Cell's Inventory");
        UpdateInventory updateInventoryController = modal.getController();
        updateInventoryController.init(cellsTable.getSelectionModel().getSelectedItem());
    }
}
