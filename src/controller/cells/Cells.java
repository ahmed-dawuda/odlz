package controller.cells;


import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.paint.Color;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.Cell;

import java.util.Arrays;
import java.util.List;

public class Cells {


    @FXML private TableView<Cell> cellsTable;
    @FXML private TableColumn<Cell, String> nameCol;
    @FXML private TableColumn<Cell, String> caretakerCol;
    @FXML private TableColumn<Cell, String> phoneCol;
    @FXML private TableColumn<Cell, Number> deleteCol;
    @FXML private TableColumn<Cell, Number> editCol;
    @FXML private TableColumn<Cell, String> statusCol;
    @FXML private TableColumn<Cell, String> idtypeCol;
    @FXML private TableColumn<Cell, String> idnumberCol;
    @FXML private TableColumn<Cell, String> landmarkCol;
    @FXML private TableColumn<Cell, String> dacCol;
    @FXML private TableColumn<Cell, String> districtCol;
    @FXML private TableColumn<Cell, String> regionCol;

    private utility.Cell cellUtil = new utility.Cell();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(nameCol, caretakerCol, phoneCol, deleteCol, editCol, statusCol, idtypeCol, idnumberCol, landmarkCol, dacCol, districtCol, regionCol),
                Arrays.asList("name", "caretaker", "phone", "id", "id", "status", "idType", "idNumber", "landmark", "dac", "district", "region"));

        deleteCol.setCellFactory(c -> new TableCell<Cell, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Delete");
                link.setOnAction(e -> {
                    cellsTable.getItems().remove(getTableRow().getIndex());
                });
                link.setTextFill(Color.valueOf(GlobalValues.ORANGE));
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        editCol.setCellFactory(c -> new TableCell<Cell, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Edit");
                link.setTextFill(Color.GREEN);
                link.setOnAction(e -> {
                    FXMLLoader modal = ViewUtility.modal("popups/createcell", "Create new cells");
                    CreateCell createCellController = modal.getController();
                    createCellController.init(cellsTable.getItems(), cellsTable.getItems().get(getTableRow().getIndex()));
                });
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        cellsTable.setItems(FXCollections.observableArrayList(cellUtil.all()));
    }

    public void createCell(){
        FXMLLoader modal = ViewUtility.modal("popups/createcell", "Create new cells");
        CreateCell createCellController = modal.getController();
        createCellController.init(cellsTable.getItems());
    }
}
