package controller.cells;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;
import model.Cell;
import model.Product;
import utility.*;
import utility.Inventory;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;

public class UpdateInventory {
    @FXML private TextField updateQty;
    @FXML private ChoiceBox<Product> product;
    @FXML private TextField instockQty;

    @FXML private TableView<model.Inventory> inventoryTable;
    @FXML private TableColumn<model.Inventory, Product> productCol;
    @FXML private TableColumn<model.Inventory, Double> instockCol;
    @FXML private TableColumn<model.Inventory, Double> updateCol;
    @FXML private TableColumn<model.Inventory, Number> controlCol;

    private utility.Inventory inventoryUtil = new Inventory();

    private Cell cell = null;

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(productCol, instockCol, updateCol, controlCol), Arrays.asList("product", "prevQty", "quantity", "id"));
        productCol.setCellFactory(c -> new TableCell<model.Inventory, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        instockCol.setCellFactory(c -> new TableCell<model.Inventory, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        updateCol.setCellFactory(c -> new TableCell<model.Inventory, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        controlCol.setCellFactory(c -> new TableCell<model.Inventory, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Remove");
                link.setTextFill(Color.valueOf(GlobalValues.ORANGE));
                setAlignment(Pos.BASELINE_CENTER);
                link.setOnAction(e -> {
                    inventoryTable.getItems().remove(getTableRow().getIndex());
                });
                setGraphic(item == null || empty ? null : link);
            }
        });

        product.setConverter(new StringConverter<Product>() {
            @Override
            public String toString(Product object) {
                return object.getName();
            }

            @Override
            public Product fromString(String string) {
                return null;
            }
        });

        product.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            model.Inventory inventoryByProduct = inventoryUtil.getCellInventoryByProduct(this.cell, product.getSelectionModel().getSelectedItem());
            instockQty.setText(GlobalValues.getDefaultDecimalFormat().format(inventoryByProduct.getQuantity()));
        });

        product.setItems(FXCollections.observableArrayList(inventoryUtil.products()));

        updateQty.setAlignment(Pos.BASELINE_RIGHT);

        updateQty.textProperty().addListener((observable, oldValue, newValue) -> {
            newValue = newValue.isEmpty() ? "0" : newValue;
            try {
                double qty = Double.parseDouble(newValue);
            } catch (Exception ex) {
                updateQty.setText(oldValue);
            }
        });

        inventoryTable.setItems(FXCollections.observableArrayList());
    }

    public void init(model.Cell cell){
        this.cell = cell;
    }

    public void update(){
        if (updateQty.getText().trim().isEmpty()) {
            ViewUtility.alert("Quantity required", "Please make sure you provide the quantity of product");
            return;
        } else if(product.getSelectionModel().getSelectedItem() == null) {
            ViewUtility.alert("Product required", "You must select a product");
            return;
        }

        List<model.Inventory> inventories = inventoryTable.getItems();
        for (model.Inventory inventory : inventories) {
            if (inventory.getProduct().getName().trim().equalsIgnoreCase(product.getSelectionModel().getSelectedItem().getName())) {
                ViewUtility.alert("Product already updated", "You have already updated this inventory product, Please remove it first");
                return;
            }
        }

        model.Inventory zoneInventoryByProduct = inventoryUtil.getZoneInventoryByProduct(GlobalValues.getZone(), product.getSelectionModel().getSelectedItem());

        if (Double.parseDouble(updateQty.getText().trim()) > zoneInventoryByProduct.getQuantity()) {
            ViewUtility.alert("Sorry, Insufficient Amount", "You have insufficient quantity in your inventory");
            return;
        }

        if (this.cell != null) {
            model.Inventory inventoryByProduct = inventoryUtil.getCellInventoryByProduct(this.cell, product.getSelectionModel().getSelectedItem());

            model.Inventory inventory = new model.Inventory();
            inventory.setId(GlobalValues.generateId());
            inventory.setLocation(cell);
            inventory.setPrevQty(inventoryByProduct.getQuantity());
            inventory.setQuantity(Double.parseDouble(updateQty.getText().trim()));
            inventory.setProduct(product.getSelectionModel().getSelectedItem());
            inventoryTable.getItems().add(inventory);
        }
    }

    public void save(){
        List<model.Inventory> list = inventoryTable.getItems();
        for (model.Inventory inventory : list) {
            model.Inventory cellInventoryByProduct = inventoryUtil.getCellInventoryByProduct(this.cell, inventory.getProduct());
            model.Inventory zoneInventoryByProduct = inventoryUtil.getZoneInventoryByProduct(GlobalValues.getZone(), inventory.getProduct());

            DBManager.begin();
            cellInventoryByProduct.setPrevQty(cellInventoryByProduct.getQuantity());
            cellInventoryByProduct.setQuantity(cellInventoryByProduct.getPrevQty() + inventory.getQuantity());
            cellInventoryByProduct.setLastUpdated(new Date(System.currentTimeMillis()));
            zoneInventoryByProduct.setQuantity(zoneInventoryByProduct.getQuantity() - inventory.getQuantity());
            DBManager.commit();
        }

        ViewUtility.closeWindow(product);
        ViewUtility.alert("Success", "You've successfully updated the cell's inventory");
    }
}
