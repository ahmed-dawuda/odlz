package controller.cells;


import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.util.StringConverter;
import mStuff.DBManager;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.*;
import model.Analysis;
import model.Cell;
import model.Product;
import utility.*;
import utility.Inventory;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

public class NewAnalysis {
    @FXML private TableView<AnalysisItem> analysisTable;
    @FXML private TableColumn<AnalysisItem, Product> productCol;
    @FXML private TableColumn<AnalysisItem, Date> dateOfLastCol;
    @FXML private TableColumn<AnalysisItem, Double> previousQtyCol;
    @FXML private TableColumn<AnalysisItem, Date> dateCurrentCol;
    @FXML private TableColumn<AnalysisItem, Double> currentStockCol;
    @FXML private TableColumn<AnalysisItem, Double> differenceCol;
    @FXML private TableColumn<AnalysisItem, Double> unitPriceCol;
    @FXML private TableColumn<AnalysisItem, Double> amountCol;
    @FXML private TableColumn<AnalysisItem, Number> editCol;

    @FXML private TextField product;
//    @FXML private DatePicker stockAsAt;
//    @FXML private DatePicker totalAsAt;
    @FXML private TextField supplyOn;
    @FXML private TextField currentStock;
    @FXML private TextField difference;
    @FXML private TextField unitPrice;
    @FXML private TextField amount;

    @FXML private ChoiceBox<model.Cell> cellCB;

    private utility.Cell cellUtil = new utility.Cell();
    private int tableIndex = 1000;

    private AnalysisItem analysisItem = null;

    utility.Analysis analysisUtil = new utility.Analysis();
    private List<Analysis> analysesTable;

    utility.Inventory inventoryUtil = new Inventory();

    CashBook cashBookUtil = new CashBook();

    public void pupulateFields(AnalysisItem item){
        product.setText(item.getProduct().getName());
//        stockAsAt.setValue(item.getStockBalAsAt().toLocalDate());
//        totalAsAt.setValue(item.getTotalStockAsAt().toLocalDate());
        supplyOn.setText(GlobalValues.getDefaultDecimalFormat().format(item.getSupplyOn()));
        currentStock.setText(GlobalValues.getDefaultDecimalFormat().format(item.getCurrentStock()));
        difference.setText(GlobalValues.getDefaultDecimalFormat().format(item.getQuantity()));
        unitPrice.setText(GlobalValues.getDefaultDecimalFormat().format(item.getUnitPrice()));
        amount.setText(GlobalValues.getDefaultDecimalFormat().format(item.getAmount()));
    }

    public void initialize(){

        currentStock.setAlignment(Pos.BASELINE_RIGHT);
        supplyOn.setAlignment(Pos.BASELINE_RIGHT);
        unitPrice.setAlignment(Pos.BASELINE_RIGHT);
        difference.setAlignment(Pos.BASELINE_RIGHT);
        amount.setAlignment(Pos.BASELINE_RIGHT);

        cellCB.setConverter(new StringConverter<Cell>() {
            @Override
            public String toString(Cell object) {
                return object.getName();
            }

            @Override
            public Cell fromString(String string) {
                return null;
            }
        });

//        cellCB.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
//            analysisTable.getItems().clear();
//            List<Analysis> analyses = newValue.getAnalyses();
//            if (analyses.size() > 0) {
//                Analysis analysis = analyses.get(analyses.size() - 1);
//                analysisTable.getItems().addAll(analysis.getAnalysisItems());
//            }
//        });

        cellCB.setItems(FXCollections.observableArrayList(cellUtil.all()));


        ViewUtility.initColumns(Arrays.asList(productCol, dateOfLastCol, previousQtyCol, dateCurrentCol, currentStockCol, differenceCol, unitPriceCol, amountCol, editCol),
                Arrays.asList("product", "stockBalAsAt", "supplyOn", "totalStockAsAt", "currentStock", "quantity", "unitPrice", "amount", "id"));

        productCol.setCellFactory(c -> new TableCell<AnalysisItem, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        dateOfLastCol.setCellFactory(c -> new TableCell<AnalysisItem, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        previousQtyCol.setCellFactory(c -> new TableCell<AnalysisItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        dateCurrentCol.setCellFactory(c -> new TableCell<AnalysisItem, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        currentStockCol.setCellFactory(c -> new TableCell<AnalysisItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        differenceCol.setCellFactory(c -> new TableCell<AnalysisItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        unitPriceCol.setCellFactory(c -> new TableCell<AnalysisItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        amountCol.setCellFactory(c -> new TableCell<AnalysisItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        editCol.setCellFactory(c -> new TableCell<AnalysisItem, Number>(){
            @Override
            protected void updateItem(Number item, boolean empty) {
                super.updateItem(item, empty);
                Hyperlink link = new Hyperlink("Edit");
                link.setTextFill(Color.GREEN);
                link.setOnAction(e -> {
                    pupulateFields(analysisTable.getItems().get(getTableRow().getIndex()));
                    tableIndex = getTableRow().getIndex();
                });
                setGraphic(item == null || empty ? null : link);
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        currentStock.textProperty().addListener((observable, oldValue, newValue) -> {
            double qty;
            if (newValue.isEmpty()) {
                difference.setText("0");
                return;
            }
            try{
                if (Double.parseDouble(currentStock.getText()) > Double.parseDouble(supplyOn.getText())) currentStock.setText(oldValue);
                double supply = Double.parseDouble(supplyOn.getText().trim());
                double current = Double.parseDouble(currentStock.getText().trim());
                difference.setText(GlobalValues.getDefaultDecimalFormat().format(supply - current));
                amount.setText(GlobalValues.getDefaultDecimalFormat().format((supply - current) * Double.parseDouble(unitPrice.getText().trim())));
            }catch (Exception ex){
                currentStock.setText(oldValue);
            }

        });

        difference.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) amount.setText(GlobalValues.getDefaultDecimalFormat().format(0));
            double supply = Double.parseDouble(supplyOn.getText().trim());
            double current = Double.parseDouble(currentStock.getText().trim());
            amount.setText(GlobalValues.getDefaultDecimalFormat().format(supply - current));
        });

        analysisTable.setItems(FXCollections.observableArrayList());
        cellCB.getSelectionModel().selectFirst();
    }

    public void save(){
//        if (stockAsAt.getValue() == null || totalAsAt.getValue() == null) return;

//        double oldstock = Double.parseDouble(supplyOn.getText());
//        Product product = analysisTable.getItems().get(tableIndex).getProduct();
//        Date stockAsAt = Date.valueOf(this.stockAsAt.getValue());
//        Date totalAsAt = new Date(System.currentTimeMillis());
//        double difference = Double.parseDouble(this.difference.getText());
//        double unitprice = Double.parseDouble(this.unitPrice.getText());
//        double amount = Double.parseDouble(this.amount.getText());
//        double current = Double.parseDouble(this.currentStock.getText());

//        AnalysisItem item = analysisUtil.createAnalysisItem(null, product, oldstock, current, stockAsAt);
//        analysisTable.getItems().remove(tableIndex);
//        analysisTable.getItems().add(item);
//        clearForm();
//        analysisTable.
    }

    public void clearForm(){
        product.clear();
        currentStock.clear();
        supplyOn.clear();
        amount.clear();
        difference.clear();
        unitPrice.clear();
//        stockAsAt.setValue(null);
//        totalAsAt.setValue(null);
    }

    public void newProduct(){
        FXMLLoader modal = ViewUtility.modal("popups/newProduct", "Add New Product");
        NewProduct newProduct = modal.getController();
        newProduct.init(analysisTable.getItems(), cellCB.getSelectionModel().getSelectedItem());
    }

    public void finish(){

        Analysis analysis = analysisUtil.createAnalysis(cellCB.getSelectionModel().getSelectedItem());

        List<AnalysisItem> analysisItems = analysisTable.getItems();
        double pay = 0;

        for (AnalysisItem item : analysisItems) {
            AnalysisItem analysisItem = analysisUtil.cloneAnalysisItem(item);
            analysisItem.setAnalysis(analysis);
            DBManager.save(AnalysisItem.class, analysisItem);
            model.Inventory cellInventoryByProduct = inventoryUtil.getCellInventoryByProduct(this.cellCB.getSelectionModel().getSelectedItem(), item.getProduct());
            //credit here
//            cashBookUtil.credit(analysisItem.getAmount(), "cash payment from cell: "+ this.cellCB.getSelectionModel().getSelectedItem().getName());
            pay += analysisItem.getAmount();
            DBManager.begin();
            cellInventoryByProduct.setPrevQty(cellInventoryByProduct.getQuantity());
            cellInventoryByProduct.setQuantity(item.getCurrentStock());
            cellInventoryByProduct.setLastUpdated(new Date(System.currentTimeMillis()));
            DBManager.commit();
        }

        cashBookUtil.credit(pay, "cash payment from cell: "+ this.cellCB.getSelectionModel().getSelectedItem().getName());

//        DBManager.begin();
        analysis.setAnalysisItems(analysisItems);
//        DBManager.commit();

        ViewUtility.closeWindow(cellCB);
        this.analysesTable.add(analysis);
        ViewUtility.alert("Success", "Analysis was made successfully");
    }

    public void init(List<Analysis> analyses, Analysis... analysisitems){
        this.analysesTable = analyses;
        if (analysisitems.length > 0) {
            cellCB.getSelectionModel().select(analysisitems[0].getCell());
        }
    }
}
