package controller.cells;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.util.StringConverter;
import mStuff.GlobalValues;
import mStuff.ViewUtility;
import model.AnalysisItem;
import model.Cell;
import model.Product;

import java.sql.Date;
import java.util.Arrays;

public class Analysis {

    public TableView<model.Analysis> cellsTable;
    public TableColumn<model.Analysis, Cell> nameCol;
    public TableColumn<model.Analysis, Date> dateCol;

    public TableView<AnalysisItem> analysisTable;

    public TableColumn<AnalysisItem, Product> productCol;
    public TableColumn<AnalysisItem, Date> dateOfLastCol;
    public TableColumn<AnalysisItem, Double> previousQtyCol;
    public TableColumn<AnalysisItem, Date> dateCurrentCol;
    public TableColumn<AnalysisItem, Double> currentStockCol;
    public TableColumn<AnalysisItem, Double> quantityCol;
    public TableColumn<AnalysisItem, Double> unitPriceCol;
    public TableColumn<AnalysisItem, Double> amountCol;

    @FXML private ChoiceBox<Cell> selectCell;

    private utility.Cell cellUtil = new utility.Cell();
    private utility.Analysis analysisUtil = new utility.Analysis();

    public void initialize(){
        ViewUtility.initColumns(Arrays.asList(nameCol, dateCol), Arrays.asList("cell", "date"));

        selectCell.setConverter(new StringConverter<Cell>() {
            @Override
            public String toString(Cell object) {
                return object.getName();
            }

            @Override
            public Cell fromString(String string) {
                return null;
            }
        });

        selectCell.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.getName().equalsIgnoreCase("All Cells")) {
                System.out.println("All cells");
                cellsTable.getItems().clear();
                cellsTable.getItems().addAll(analysisUtil.analyses());
                cellsTable.getSelectionModel().selectFirst();
            } else {
                cellsTable.getItems().clear();
                cellsTable.getItems().addAll(analysisUtil.analysesByCell(newValue));
                cellsTable.getSelectionModel().selectFirst();
            }
        });
        Cell cell = new Cell();
        cell.setName("All Cells");

        selectCell.setItems(FXCollections.observableArrayList(cell));
        selectCell.getItems().addAll(cellUtil.all());

        nameCol.setCellFactory(c -> new TableCell<model.Analysis, Cell>(){
            @Override
            protected void updateItem(Cell item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        dateCol.setCellFactory(c -> new TableCell<model.Analysis, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
            }
        });

        cellsTable.setItems(FXCollections.observableArrayList(analysisUtil.analyses()));

        ViewUtility.initColumns(
                Arrays.asList(productCol, dateOfLastCol, previousQtyCol, dateCurrentCol, currentStockCol, quantityCol, unitPriceCol, amountCol),
                Arrays.asList("product", "stockBalAsAt", "supplyOn", "totalStockAsAt", "currentStock", "quantity", "unitPrice", "amount"));


        productCol.setCellFactory(c -> new TableCell<AnalysisItem, Product>(){
            @Override
            protected void updateItem(Product item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : item.getName());
            }
        });

        dateOfLastCol.setCellFactory(c -> new TableCell<AnalysisItem, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        previousQtyCol.setCellFactory(c -> new TableCell<AnalysisItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        dateCurrentCol.setCellFactory(c -> new TableCell<AnalysisItem, Date>(){
            @Override
            protected void updateItem(Date item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDateInstance().format(item));
                setAlignment(Pos.BASELINE_CENTER);
            }
        });

        currentStockCol.setCellFactory(c -> new TableCell<AnalysisItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        quantityCol.setCellFactory(c -> new TableCell<AnalysisItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getDefaultDecimalFormat().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        unitPriceCol.setCellFactory(c -> new TableCell<AnalysisItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        amountCol.setCellFactory(c -> new TableCell<AnalysisItem, Double>(){
            @Override
            protected void updateItem(Double item, boolean empty) {
                super.updateItem(item, empty);
                setText(item == null || empty ? null : GlobalValues.getCurrencyInstance().format(item));
                setAlignment(Pos.BASELINE_RIGHT);
            }
        });

        analysisTable.setItems(FXCollections.observableArrayList());

        cellsTable.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            analysisTable.getItems().clear();
            analysisTable.getItems().addAll(newValue.getAnalysisItems());
//            System.out.println(newValue);
        });

        cellsTable.getSelectionModel().selectFirst();
    }

    public void newAnalysis(){
        FXMLLoader modal = ViewUtility.modal("popups/cellanalysisnewproduct", "Create new Analysis");
        NewAnalysis newAnalysis = modal.getController();
        newAnalysis.init(cellsTable.getItems(), cellsTable.getSelectionModel().getSelectedItem());
    }
}
