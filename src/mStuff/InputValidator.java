package mStuff;

import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

import java.io.File;

public class InputValidator {

    public enum NodeType {TEXTFIELD, DATEPICKER, CHOICEBOX, INTEGERFIELD, DOUBLEFIELD}
    private static final String ERROR_BORDER = "m-border-red";

    public static boolean validateBasic(Node node, NodeType type, String label) {
        boolean valid = false;
        switch (type) {
            case CHOICEBOX:
                valid = choiceBox((ChoiceBox) node);
                break;
            case TEXTFIELD:
                valid = textField((TextField) node);
                break;
            case DATEPICKER:
                valid = datePicker(((DatePicker) node));
                break;
            case DOUBLEFIELD:
                valid = decimalField((TextField) node);
                break;
            case INTEGERFIELD:
                valid = integerField((TextField) node);
                break;
            default:
                break;
        }
        if (!valid) {
            Platform.runLater(() -> new Alert(Alert.AlertType.ERROR, "Invalid input for " + label).show());
        }
        return valid;
    }

    public static boolean validateFile(File file, String label) {
        boolean valid = file != null && !file.getName().isEmpty();
        if (!valid)
            Platform.runLater(() -> new Alert(Alert.AlertType.ERROR, "Invalid input for " + label).show());

        return valid;
    }

    public static boolean textField(TextField field) {
        return field != null && !field.getText().trim().isEmpty();
    }

    public static boolean usernameField(TextField field) {
        return field != null && !field.getText().trim().isEmpty() && field.getText().trim().matches("^[a-zA-Z0-9]+$");
    }

    public static boolean integerField(TextField field) {
        return field != null && !field.getText().trim().isEmpty() && field.getText().trim().matches("^-?\\d+$");
    }

    public static boolean decimalField(TextField field) {
        return field != null && !field.getText().trim().isEmpty() && field.getText().trim().matches("^[0-9]+(\\.[0-9]*)?$");
    }

    public static boolean datePicker(DatePicker picker) {
        return picker != null && picker.getValue() != null;
    }

    public static boolean choiceBox(ChoiceBox box) {
        return box != null && box.getValue() != null;
    }
}
