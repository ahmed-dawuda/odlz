package mStuff;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Control;
import javafx.scene.control.TextField;

import java.util.HashMap;
import java.util.Map;

public class FxForm<T extends Object> {

    private Map<String, Control> input;

    public FxForm() {
        this.input = new HashMap<>();
    }

    public void addInput(String name, Control control) {
        input.put(name, control);
    }

    private boolean validate() {
        return true;
    }

    public T submit() {
        return null;
    }

    public String toJsonString() {
        StringBuilder builder = new StringBuilder("{");
        for (String s : input.keySet()) {
            builder.append(s).append(":").append(input.get(s)).append(",");
        }
        builder.deleteCharAt(builder.lastIndexOf(","));
        builder.append("}");

        return builder.toString();
    }

    private String getValue(Control control) {
        if (control instanceof TextField)
            return ((TextField) control).getText().trim();
        else if (control instanceof ChoiceBox)
            return ((ChoiceBox) control).getValue().toString();

        return null;
    }
}
