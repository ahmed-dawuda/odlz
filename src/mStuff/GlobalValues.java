package mStuff;


import model.Product;
import model.Worker;
import model.Zone;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GlobalValues {
    public static Number generateId(){
        return System.currentTimeMillis();
    }

    public static DecimalFormat getDefaultDecimalFormat() {
        return new DecimalFormat("#0");
    }

    public static DecimalFormat getCurrencyInstance() {
        return new DecimalFormat("¢###,###,##0.00");
    }

    public static DecimalFormat getCommaSeparatedInstance() {
        return new DecimalFormat("###,###,##0.00");
    }

    public static DateFormat getDateInstance() {
        return new SimpleDateFormat("E, dd MMM yyyy");
    }

    public static DateFormat getTimeInstance() {
        return new SimpleDateFormat("hh:mm aa");
    }

    public static final String ORANGE = "#e65100";

    public static final List<String> months = Arrays.asList("JANUARY", "FEBRUARY", "MARCH", "ARPIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER");

    public static Worker worker = null;

    public static Zone zone = null;

    public static Worker getWorker() {
        return worker;
    }

    public static void setWorker(Worker worker) {
        GlobalValues.worker = worker;
    }

    public static Zone getZone() {
        return zone;
    }

    public static void setZone(Zone zone) {
        GlobalValues.zone = zone;
    }
}
